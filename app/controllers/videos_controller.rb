# frozen_string_literal: true

class VideosController < ApplicationController
  before_action :find_video, only: %i[show edit update destroy]

  def index
    @videos = Video.all
  end

  def show; end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new(video_params)
    if @video.save
      flash[:success] = 'Видео обрабатывается 30сек!'
      ConverterWorker.perform_async(@video.id)
      redirect_to video_path(@video)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @video.update(video_params)
      ::ConverterWorker.perform_async(@video.id)
      redirect_to :show
    else
      render :edit
    end
  end

  def destroy
    @video.destroy
    redirect_to videos_path
  end

  private

  def find_video
    @video = Video.find(params[:id])
  end

  def video_params
    params.require(:video).permit(:attachment)
  end
end
