# frozen_string_literal: true

class Video < ApplicationRecord
  mount_uploader :attachment, VideoUploader

  def set_success(format, opts)
    self.success = true
  end
end
