# frozen_string_literal: true

class ConverterWorker
  include Sidekiq::Worker

  def perform(video_id)
    ConvertVideoToGifService.new(video_id).process
  end
end
