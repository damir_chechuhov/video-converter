# frozen_string_literal: true

class ConvertVideoToGifService
  attr_reader :video_id

  RESULT_PATH = 'public/uploads/tmp/result.gif'

  def initialize(video_id)
    @video_id = video_id
  end

  def process
    return unless video

    run_process
    save_gif
  ensure
    FileUtils.rm_rf(full_path)
  end

  private

  def run_process
    path = video.attachment.file.path
    system("ffmpeg -ss 0:00:00 -t 00:00:30 -i #{path} #{RESULT_PATH}")
  end

  def save_gif
    File.open(full_path) { |f| video.attachment = f }
    video.save!
  end

  def full_path
    @full_path ||= File.join(Rails.root, RESULT_PATH)
  end

  def video
    p @video ||= Video.find_by(id: video_id)
  end
end
