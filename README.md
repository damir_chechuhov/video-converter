# Video converter
This app converts video to GIF.
## Used technologies
* Rails 5.2.6
* Ruby 2.6.3
* PostgreSQL
* Puma
* ffmpeg lib
## Getting Started
Install [RVM](https://rvm.io/) with Ruby 2.6.3.
##### Install ffmpeg for macOS:
```
brew install ffmpeg
```
##### Install ffmpeg for Ubuntu:
```
sudo apt install ffmpeg
```
##### Install gems:
```
gem install bundler
bundle install
```
##### Install DB:
```
rake db:create
rake db:migrate
```